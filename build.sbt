name := "agenty-2015"

version := "0.1.0"

scalaVersion := "2.11.5"

sbtVersion := "0.13.7"

fork in run := true

libraryDependencies ++= Seq(
	"com.jidesoft" % "jidefx-common" % "0.9.1",
	"com.jidesoft" % "jidefx-fields" % "0.9.1",
	"com.jidesoft" % "jidefx-decoration" % "0.9.1",

	"com.softwaremill.macwire" % "macros_2.11" % "0.8.0",
	"com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % "2.5.1"
	
)
