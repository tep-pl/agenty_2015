package agenty.gui.fxextensions

import scala.language.implicitConversions

object SpecializedProperties {
	
	import javafx.beans.property._
	import java.lang._
	
	private val bindings = collection.mutable.ListBuffer[Property[_]]()
	private def storing[T <: Property[_]](t: T) = { bindings += t; t}
	
	private type OP[T] = ObjectProperty[T]
	private type IP = IntegerProperty
	private type LP = LongProperty
	private type DP = DoubleProperty
	private type FP = FloatProperty
	private type BP = BooleanProperty
	
	private def mkIP = IntegerProperty.integerProperty _
	private def mkLP = LongProperty.longProperty _
	private def mkDP = DoubleProperty.doubleProperty _
	private def mkFP = FloatProperty.floatProperty _
	private def mkBP = BooleanProperty.booleanProperty _
	
	implicit def intTypedToObjectProperty(o: IP) = o.asObject()
	implicit def intObjectToTypedProperty(o: OP[Integer]) = storing(mkIP(o))
	
	implicit def longTypedToObjectProperty(o: LP) = o.asObject()
	implicit def longObjectToTypedProperty(o: OP[Long]) = storing(mkLP(o))
	
	implicit def doubleTypedToObjectProperty(o: DP) = o.asObject()
	implicit def doubleObjectToTypedProperty(o: OP[Double]) = storing(mkDP(o))
	
	implicit def floatTypedToObjectProperty(o: FP) = o.asObject()
	implicit def floatObjectToTypedProperty(o: OP[Float]) = storing(mkFP(o))
	
	implicit def booleanTypedToObjectProperty(o: BP) = o.asObject()
	implicit def booleanObjectToTypedProperty(o: OP[Boolean]) = storing(mkBP(o))
		
}
