package agenty.gui.fxextensions

import scala.language.{implicitConversions, higherKinds}

object CovariantProperty {
	
	// javafx Property is invariant in T
	// (Property[Double] IS NOT Property[Number])
	
	import javafx.beans.property._
	
	implicit def toCovariantProperty[T, P[T] <: Property[_]](p: P[_ <: T]): P[T] = p.asInstanceOf[P[T]]
}
