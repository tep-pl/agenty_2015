package agenty.gui.fxextensions

object BindingOperators {
	
	implicit class ExtProperty[T](p: javafx.beans.property.Property[T]) {
		
		def <==(q: javafx.beans.value.ObservableValue[T]) 	= p.bind(q)
		def <=>(q: javafx.beans.property.Property[T]) 		= p.bindBidirectional(q)
		def ==>(q: javafx.beans.property.Property[T]) 		= q.bind(p)
	}
	
	implicit class ExtObservableValue[T](p: javafx.beans.value.ObservableValue[T]) {
		
		def ==>(q: javafx.beans.property.Property[T]) = q.bind(p)
	}
}
