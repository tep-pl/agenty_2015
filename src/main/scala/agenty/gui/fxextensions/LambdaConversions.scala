package agenty.gui.fxextensions

import scala.language.implicitConversions

object LambdaConversions {
	
	implicit def blockToRunnable(f: => Any): Runnable = new Runnable() {
		override def run() = f
	}
	
	implicit def lambdaToRunnable(f: Function0[Any]): Runnable = new Runnable() {
		override def run() = f()
	}
	
	import javafx.event._
	
	implicit def blockToEventHandler[T <: Event](f: => Any): EventHandler[T] = new EventHandler[T] {
		override def handle(e: T) = f
	}
	
	implicit def lambdaToEventHandler[T <: Event](f: Function0[Any]): EventHandler[T] = new EventHandler[T] {
		override def handle(e: T) = f()
	}
	
	implicit def lambdaToEventHandler[T <: Event](f: Function[T, Any]): EventHandler[T] = new EventHandler[T] {
		override def handle(e: T) = f(e)
	}
	
	import javafx.beans.value._
	import javafx.beans._
	import java.lang._
	
	implicit def lambdaToChangelistener[
		T, 								// type of the observable, e.g. for ObservableIntegerValue T is Number (!)
		U <: T, 						// closest type, e.g. for ObservableIntegerValue we want Integer here
		S <: ObservableValue[_ <: T]] 	// observable, e.g. ObservableValue[Number], ObservableValue[Integer] or ObservableIntegerValue
	(f: Function3[S, U, U, Any]) : ChangeListener[_ >: T] = new ChangeListener[T] {
		// signature must match ChangeListener interface, but f provides more accurate signature, thus casts are necessary (but safe)
		override def changed(o: ObservableValue[_ <: T], v1: T, v2: T) = f(o.asInstanceOf[S], v1.asInstanceOf[U], v2.asInstanceOf[U])
	}
	
	// in case one is not interested in previous value
	implicit def lambdaToChangelistener[T, U <: T, S <: ObservableValue[_ <: T]]
	(f: Function2[S, U, Any]) : ChangeListener[_ >: T] = new ChangeListener[T] {
		override def changed(o: ObservableValue[_ <: T], v1: T, v2: T) = f(o.asInstanceOf[S], v2.asInstanceOf[U])
	}
	
	// in case one cares only about new value
	implicit def lambdaToChangelistener[T, U <: T, S <: ObservableValue[_ <: T]]
	(f: Function1[U, Any]) : ChangeListener[_ >: T] = new ChangeListener[T] {
		override def changed(o: ObservableValue[_ <: T], v1: T, v2: T) = f(v2.asInstanceOf[U])
	}
	
	// just notification
	implicit def lambdaToChangelistener[T, U <: T, S <: ObservableValue[_ <: T]]
	(f: Function0[Any]) : ChangeListener[_ >: T] = new ChangeListener[T] {
		override def changed(o: ObservableValue[_ <: T], v1: T, v2: T) = f()
	}
	
	implicit def blockToChangelistener[T, U <: T, S <: ObservableValue[_ <: T]]
	(f: => Any) : ChangeListener[_ >: T] = new ChangeListener[T] {
		override def changed(o: ObservableValue[_ <: T], v1: T, v2: T) = f
	}
}
