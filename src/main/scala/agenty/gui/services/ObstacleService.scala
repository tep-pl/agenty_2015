package agenty.gui.services

import javafx.scene.input.MouseEvent
import com.softwaremill.macwire.MacwireMacros._
import javafx.scene.layout.Pane
import agenty.utils.PolygonShapeBuilder
import javafx.scene.input.MouseButton
import javafx.geometry.Point2D
import javafx.scene.paint.Color
import agenty.gui.models.Obstacle
import collection.mutable.ListBuffer
import javafx.fxml.FXML
import agenty.utils.PolygonShapeBuilder

class ObstacleService {
  
    lazy val polyBuilder = wire [PolygonShapeBuilder]
  
    var selectedCorners = ListBuffer[Point2D]()
    var canvas: Pane = new Pane()

    def handleObstacleCreationClick(e: MouseEvent): Obstacle = {
      var obstacle: Obstacle = null
      if (canvas.getBoundsInParent.contains(e.getX, e.getY)) {
        e.getButton match {
            case MouseButton.PRIMARY => handleLeftClick(e)
            case MouseButton.SECONDARY => obstacle = handleRightClick(e)
            case _ => 'pass
        }
      }
      obstacle
    }
    
    def handleLineUpdate(p: Point2D) = {
      polyBuilder updateLine Common.Point2D(p.getX, p.getY)
    }
      
    private def handleLeftClick(e: MouseEvent) = {
      val corner = canvas.parentToLocal(e.getX, e.getY)
      selectedCorners += corner
      
      if (polyBuilder.state == PolygonShapeBuilder.State.Idle) {
        val ln = polyBuilder.polyline
        ln.setStroke(Color.RED)
        ln.setStrokeWidth(0.5)
        canvas.getChildren.add(ln)
      }
    
      polyBuilder addPoint Common.Point2D(corner.getX, corner.getY)
    }
    
    private def handleRightClick(e: MouseEvent): Obstacle = { 
      if (polyBuilder.state == PolygonShapeBuilder.State.Building) {
        val poly = polyBuilder.polygon
        poly.setFill(Color.TRANSPARENT)
        poly.setFill(Color.BROWN)
        poly.setStroke(Color.DARKBLUE)
        poly.setStrokeWidth(0.5)
    
        canvas.getChildren.add(poly)
        canvas.getChildren.removeAll(polyBuilder.polyline)
        
        val obstacle = new Obstacle()
        obstacle.corners ++= selectedCorners
        selectedCorners.clear()
  
        polyBuilder.reset
        return obstacle
      }
      null
    }
    
    
}