package agenty.gui.services

import java.io.File

import agenty.gui.models.Agent
import agenty.gui.models.Obstacle
import collection.mutable.ListBuffer
import agenty.gui.models.AgentDTO
import agenty.gui.models.World
import agenty.gui.models.WorldDTO
import java.io.PrintWriter
import agenty.gui.models.ObstacleDTO
import agenty.gui.models.Point2DDTO
import javafx.geometry.Point2D
import javafx.scene.paint.Color
import com.softwaremill.macwire.MacwireMacros._
import javafx.scene.layout.Pane
import agenty.utils.PolygonShapeBuilder
import agenty.gui.controllers.AgentPanel
import javafx.scene.input.MouseEvent
import agenty.gui.controllers.Mouse

class SerializationService {

  lazy val polyBuilder = wire[PolygonShapeBuilder]
  var canvas: Pane = new Pane()
  var agentPanel: AgentPanel = new AgentPanel()

  def serializeWorld(output: File, world: World) = {
    var agentDTOs: ListBuffer[AgentDTO] = world.agents.map(x => new AgentDTO(x.name.getValue(), x.size.doubleValue(), x.color.get().toString(), x.centerX.doubleValue(), x.centerY.doubleValue(), x.targetX.doubleValue(), x.targetY.doubleValue()))
    var obstacleDTOs: ListBuffer[ObstacleDTO] = world.obstacles.map(x => new ObstacleDTO(x.corners.map(c => new Point2DDTO(c.getX(), c.getY()))))
    var worldMap: WorldDTO = new WorldDTO(agentDTOs, obstacleDTOs)

    val pw = new PrintWriter(output)
    pw.write(JacksonObjectWrapper.serialize(worldMap))
    pw.close()
  }

  def deserializeWorld(input: File): World = {
    //get from file
    val fileLines: String = scala.io.Source.fromFile(input.getAbsolutePath()).mkString
    val worldMap: WorldDTO = JacksonObjectWrapper.deserialize[WorldDTO](fileLines)

    //map from DTOs to real objects
    def agentMapping: AgentDTO => Agent = {
      case a =>
    	  new Agent(a.name, a.size, Color.valueOf(a.color), a.centerX, a.centerY, a.targetX, a.targetY)
    }

    def obstaclesMapping: ObstacleDTO => Obstacle = {
      case (o) => new Obstacle(o.corners.map(c => new Point2D(c.x, c.y)))
    }

    var agents: ListBuffer[Agent] = worldMap.agents.map(agentMapping)
    var obstacles: ListBuffer[Obstacle] = worldMap.obstacles.map(obstaclesMapping)

    // clear canvas if needed
    if(!agents.isEmpty || !obstacles.isEmpty) 
      canvas.getChildren().clear()
      
    // draw agents
    for (agent <- agents) {
      canvas.getChildren.addAll(agent.fxCircle, agent.fxTarget, agent.fxPolyline)
      agentPanel.agent setValue agent
    }
      
    // draw obstacles
    for (obstacle <- obstacles) {
      val ln = polyBuilder.polyline
      ln.setStroke(Color.RED)
      ln.setStrokeWidth(0.5)
      canvas.getChildren.add(ln)

      for (corner <- obstacle.corners)
        polyBuilder addPoint Common.Point2D(corner.getX, corner.getY)

      if (polyBuilder.state == PolygonShapeBuilder.State.Building) {
        val poly = polyBuilder.polygon
        poly.setFill(Color.TRANSPARENT)
        poly.setFill(Color.BROWN)
        poly.setStroke(Color.DARKBLUE)
        poly.setStrokeWidth(0.5)
        
        canvas.getChildren.add(poly)
        canvas.getChildren.removeAll(polyBuilder.polyline)
        
        polyBuilder.reset
      }
    }

    val world = new World(agents, obstacles)
    
    agents foreach { a =>
    	a.world = world
    	
    	// force path recalculation
    	val oldTargetX = a.targetX.get
    	a.targetX set 0
    	a.targetX set oldTargetX
    }
    
    world
  }

}