package agenty.gui.models

import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.scene.paint.Color
import javafx.beans.property.DoubleProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.scene.shape.Circle
import agenty.gui.fxextensions.BindingOperators._
import agenty.gui.fxextensions.CovariantProperty._
import agenty.gui.controllers.Draggable
import agenty.gui.fxextensions.LambdaConversions._
import scala.collection.JavaConversions._
import scala.language.implicitConversions._
import javafx.scene.paint.Paint
import javafx.scene.shape.Polyline
import javafx.concurrent.Service
import javafx.concurrent.Task
import scala.collection.mutable.ListBuffer
import javafx.scene.Cursor
import agenty.utils.VisibilityGraph
import javafx.scene.shape.MoveTo
import javafx.scene.shape.LineTo
import Common._
import agenty.utils.Hull
import javafx.scene.shape.Line
import scala.language.reflectiveCalls
import scala.language.postfixOps

class Agent(_name: String, _size: Double, _color: Color,
	_centerX: Double, _centerY: Double, _targetX: Double, _targetY: Double) {

	var world: World = null

	var name: StringProperty = new SimpleStringProperty()
	var size: DoubleProperty = new SimpleDoubleProperty()
	var color: ObjectProperty[Color] = new SimpleObjectProperty()

	var centerX: DoubleProperty = new SimpleDoubleProperty(_centerX)
	var centerY: DoubleProperty = new SimpleDoubleProperty(_centerY)

	val targetX: DoubleProperty = new SimpleDoubleProperty(_targetX)
	val targetY: DoubleProperty = new SimpleDoubleProperty(_targetY)

	/*
	 * do not change properties of fxTarget and fxCircle directly
	 * use properties exposed by Agent interface instead
	 */

	val fxTarget = new Circle with Draggable {
		val actionX = this.centerXProperty
		val actionY = this.centerYProperty
		this.setFill(Color.RED)
		this.setRadius(5)
	}

	val fxCircle = new Circle with Draggable {
		val actionX = this.centerXProperty
		val actionY = this.centerYProperty
		this.setCursor(Cursor.HAND)
	}

	val fxPolyline = new Polyline

	private type TaskProductType = java.util.List[java.lang.Double]
	val pathBuilderService = new Service[TaskProductType] {
		// run expensive path computation on separate thread
		def createTask() = new Task[TaskProductType] {
			def call(): TaskProductType = {
				
				// this service is triggered when centerXY or targetXY changes
				// to prevent recalculating path gazillion times during mouse drag,
				// we sleep here, so service may be canceled
				
				Thread.sleep(1000)

				val ll = Option(world) map { world =>
					val xxxx: Seq[Double] = buildPath()
					xxxx map {new java.lang.Double(_)} toList
				} getOrElse List[java.lang.Double]()

				ll
//				List(centerX.get, centerY.get, targetX.get, targetY.get) map {
//					new java.lang.Double(_)
//				}
			}
		}
	}

	/* setup bindings */

	fxCircle.radiusProperty bind size
	fxCircle.fillProperty bind color

	centerX bindBidirectional fxCircle.centerXProperty
	centerY bindBidirectional fxCircle.centerYProperty

	targetX bindBidirectional fxTarget.centerXProperty
	targetY bindBidirectional fxTarget.centerYProperty

	centerX.addListener(() => pathBuilderService.restart)
	centerY.addListener(() => pathBuilderService.restart)
	targetX.addListener(() => pathBuilderService.restart)
	targetY.addListener(() => pathBuilderService.restart)

	pathBuilderService.valueProperty.addListener((newPath: TaskProductType) =>
		Option(newPath) map { newPath =>
			fxPolyline.getPoints.setAll(newPath)
		})

	name set _name
	size set _size
	color set _color
	centerX set _centerX
	centerY set _centerY
	targetX set _targetX
	targetY set _targetY

	def getCircle(point: Point2D, radius: Double, precision: Int = 20): IndexedSeq[Point2D] = {
		(1 to precision) map {
			i =>
				Point2D(point.x1 + radius * Math.sin(2 * i * Math.PI / precision),
					point.x2 + radius * Math.cos(2 * i * Math.PI / precision))
		}
	}

	def thicken(poly: Polygon2D, radius: Double): Polygon2D = {
		Polygon2D(Hull.jarvis(poly.points.map(p => getCircle(p, radius)).flatten.toList))
	}

	private def buildPath(
			startX: Double = centerX.get,
			startY: Double = centerY.get,
			moreObstacles: Seq[Obstacle] = Nil
			): Seq[Double] = {

		val startMarker = new {
			val point = Point2D(startX, startY)
		}

		val targetMarker = new {
			val point = Point2D(targetX.get, targetY.get)
		}

		val polygons = (world.obstacles ++ moreObstacles) map { o =>
			Common.Polygon2D(o.corners map { c => Point2D(c.getX, c.getY) } toList)
		}

		val edges = VisibilityGraph.visibilityGraph(List(startMarker.point, targetMarker.point), polygons map { thicken(_, size.get) })

		val path = VisibilityGraph.shortestPath(edges, startMarker.point, targetMarker.point)
		
		path map { e =>
			List(e.p1.x1, e.p1.x2, e.p2.x1, e.p2.x2)
		} flatten
	}
	
	def calcPathOnDemand(startX: Double, startY: Double, moreObstacles: Seq[Obstacle]) =
		fxPolyline.getPoints.setAll(buildPath(startX, startY, moreObstacles).map(d => new java.lang.Double(d)))
}
