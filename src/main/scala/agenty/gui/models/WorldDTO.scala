package agenty.gui.models

import collection.mutable.ListBuffer

case class WorldDTO(agents: ListBuffer[AgentDTO], obstacles: ListBuffer[ObstacleDTO]) {
  

}