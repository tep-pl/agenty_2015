package agenty.gui.models

import javafx.beans.property.IntegerProperty
import collection.mutable.ListBuffer
import javafx.geometry.Point2D
import javafx.beans.property.SimpleIntegerProperty
import scala.collection.immutable.List;

case class Obstacle(corners: ListBuffer[Point2D]) {
  
  def this() {
    this(new ListBuffer())
  } 

}