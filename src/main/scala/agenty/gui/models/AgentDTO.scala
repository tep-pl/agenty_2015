package agenty.gui.models

import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.DoubleProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.scene.shape.Circle
import agenty.gui.fxextensions.BindingOperators._
import agenty.gui.fxextensions.CovariantProperty._
import agenty.gui.controllers.Draggable
import agenty.gui.fxextensions.LambdaConversions._
import scala.collection.JavaConversions._
import scala.language.implicitConversions._
import javafx.scene.paint.Paint
import javafx.scene.shape.Polyline
import javafx.concurrent.Service
import javafx.concurrent.Task
import scala.collection.mutable.ListBuffer
import javafx.scene.Cursor

case class AgentDTO(name: String, size: Double, color: String,
		centerX: Double, centerY: Double, targetX: Double, targetY: Double) {
  
}
