package agenty.gui.models

import collection.mutable.ListBuffer

case class World(agents: ListBuffer[Agent], obstacles: ListBuffer[Obstacle]) {
  
  def this() {
    this(new ListBuffer[Agent], new ListBuffer[Obstacle])
  }

}