package agenty.gui.models

import collection.mutable.ListBuffer

case class ObstacleDTO(corners: ListBuffer[Point2DDTO]) {

}