package agenty.gui

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;

class App extends Application {
	
	override def start(primaryStage: Stage) {
		
		val viewLocation = getClass().getResource("/agenty/gui/views/Main.fxml")
		val root: javafx.scene.Parent = FXMLLoader.load(viewLocation)
		
		primaryStage.setScene(new Scene(root))
		primaryStage.setTitle("agenty-2015")
		primaryStage.show
	}
}

object App {
	def main(args: Array[String]) {
		Application.launch(classOf[App], args: _*)
	}
}
