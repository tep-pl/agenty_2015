package agenty.gui.controls

import javafx.beans.property.SimpleDoubleProperty
import jidefx.scene.control.field.NumberField
import jidefx.scene.control.field.verifier.NumberRangePatternVerifier

import agenty.gui.fxextensions.BindingOperators._
import agenty.gui.fxextensions.LambdaConversions._

class SpinnerField(
		private val delta: Double = 1.0,
		private val min: Double = java.lang.Double.NEGATIVE_INFINITY,
		private val max: Double = java.lang.Double.POSITIVE_INFINITY)
	extends javafx.scene.layout.StackPane {
	
	def this() = this(
			1.0,
			java.lang.Double.NEGATIVE_INFINITY,
			java.lang.Double.POSITIVE_INFINITY)
	
	type Double = java.lang.Double
	
	val minValue 		= new SimpleDoubleProperty(min)
	val maxValue 		= new SimpleDoubleProperty(max)
	val step 			= new SimpleDoubleProperty(delta)
	
	val valueProperty 	= new SimpleDoubleProperty(0)
	
	private val innerField = new NumberField
	innerField.disableProperty 	<== disableProperty
	innerField.valueProperty	<=> valueProperty
	innerField.setPrefWidth(96.0)
	
	private def updateVerifier: () => Unit = () => {
		innerField.setPattern("n")
		innerField.getPatternVerifiers().clear()
		innerField.getPatternVerifiers().put("n",
				new NumberRangePatternVerifier(minValue.get, maxValue.get, step.get))
	}
	
	minValue.addListener(updateVerifier)
	maxValue.addListener(updateVerifier)
	step.addListener(updateVerifier)
	
	getChildren.add(innerField.asSpinner)
	updateVerifier()
}
