package agenty.gui.logic

import agenty.gui.models.World
import agenty.gui.models.Agent
import javafx.animation.Transition

trait SimulationControl {
	
	type AgentPath = Seq[Common.Segment2D]
	type SimulationState = Seq[(Agent, AgentPath)]
	
	def runSimulation(world: World, initialState: SimulationState): Unit
}
