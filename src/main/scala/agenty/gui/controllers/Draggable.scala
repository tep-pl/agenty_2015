package agenty.gui.controllers

import javafx.scene.Node
import agenty.gui.fxextensions.LambdaConversions._
import javafx.beans.property.DoubleProperty
import javafx.scene.input.MouseEvent
import javafx.scene.Cursor
import javafx.geometry.Point2D

trait Draggable {
	this: Node =>
		
	val actionX: DoubleProperty
	val actionY: DoubleProperty
		
	private var dragging = false
	private var delta = new Point2D(0, 0)
	
	// FIXME: this is never called
	this.setOnMousePressed((e: MouseEvent) => {
		dragging = true
		delta = new Point2D(actionX.get - e.getX, actionY.get - e.getY)
		this.getScene.setCursor(Cursor.MOVE)
		e.consume
	})
	
	this.setOnMouseReleased((e: MouseEvent) => {
		dragging = false
		this.getScene.setCursor(Cursor.DEFAULT)
		e.consume
	})

	this.setOnMouseDragged((e: MouseEvent) => {
		actionX.set(e.getX + delta.getX)
		actionY.set(e.getY + delta.getY)
		e.consume
	})
}
