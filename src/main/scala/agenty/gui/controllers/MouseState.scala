package agenty.gui.controllers

import javafx.scene.Cursor

object Mouse {

	sealed trait MouseMode {
		val statusText: String
		val cursor: Cursor
	}

	object DefaultMode extends MouseMode {
		val statusText = ""
		val cursor = Cursor.DEFAULT
	}

	object AddAgentMode extends MouseMode {
		val statusText = "click on canvas to create new agent"
		val cursor = Cursor.CROSSHAIR
	}

	object SetTargetMode extends MouseMode {
		val statusText = "click on canvas to mark target point"
		val cursor = Cursor.DEFAULT
	}

	object AddObstacleMode extends MouseMode {
		val statusText = "click multiple points on canvas to create an obstacle"
		val cursor = Cursor.HAND
	}
}
