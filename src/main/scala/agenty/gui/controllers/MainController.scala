package agenty.gui.controllers

import javafx.fxml.FXML
import javafx.event.ActionEvent
import javafx.fxml.Initializable
import java.net.URL
import java.util.ResourceBundle
import agenty.gui.models.Agent
import agenty.gui.models.Obstacle
import javafx.scene.control.Label
import javafx.scene.layout.Pane
import javafx.scene.layout.BorderPane
import javafx.scene.transform.Transform
import javafx.collections.ListChangeListener
import javafx.scene.Node
import javafx.collections.ListChangeListener.Change
import javafx.scene.Cursor
import javafx.scene.input.MouseEvent
import javafx.scene.paint.Color
import agenty.gui.fxextensions.LambdaConversions._
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import com.softwaremill.macwire.MacwireMacros._
import agenty.gui.services.ObstacleService
import collection.mutable.ListBuffer
import agenty.gui.services.SerializationService
import javafx.stage.FileChooser
import java.io.File
import agenty.gui.models.World
import javafx.beans.value.ObservableValue
import javafx.scene.shape.Circle
import javafx.util.Duration
import javafx.scene.control.MenuBar
import scala.math.sqrt
import scala.math.ceil
import javafx.scene.shape.Polyline
import scala.language.postfixOps
import agenty.gui.logic.SimulationControl

class MainController extends Initializable {

  lazy val obstacleService = wire[ObstacleService]
  lazy val serializationService = wire[SerializationService]

  var world: World = new World()

  @FXML var root: BorderPane = _
  @FXML var rootCanvas: Pane = _
  @FXML var agentPanel: AgentPanel = _
  @FXML var lblStatus: Label = _
  @FXML var menuBar: MenuBar = _

  val mouseMode: ObjectProperty[Mouse.MouseMode] = new SimpleObjectProperty(Mouse.DefaultMode)
  val prevMode: ObjectProperty[Mouse.MouseMode] = new SimpleObjectProperty(Mouse.DefaultMode)

  mouseMode.addListener((bean: ObservableValue[Mouse.MouseMode], oldMode: Mouse.MouseMode, newMode: Mouse.MouseMode) => {
    lblStatus setText newMode.statusText
    rootCanvas setCursor newMode.cursor
    prevMode set oldMode
  })

  @FXML
  def pressedAddAgent(e: ActionEvent): Unit = {
    mouseMode set Mouse.AddAgentMode
  }

  @FXML
  def pressedAddObstacle(e: ActionEvent): Unit = {
    mouseMode set Mouse.AddObstacleMode
  }

  @FXML
  def pressedStartSimulation(e: ActionEvent): Unit = {
    runSimluation()
  }

  @FXML
  def clearWorld(e: ActionEvent): Unit = {
    rootCanvas.getChildren().clear()
    world = new World()
  }

  @FXML
  def serializeWorldAsNewFile(e: ActionEvent): Unit = {
    var fileChooser: FileChooser = new FileChooser()
    var jsonFilter: FileChooser.ExtensionFilter = new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json");

    fileChooser.setTitle("Save your world as...")
    fileChooser.getExtensionFilters().add(jsonFilter)

    var output: File = fileChooser.showSaveDialog(root.getScene().getWindow())
    if (!output.getName().contains("."))
      output = new File(output.getAbsolutePath() + ".json")

    serializationService.serializeWorld(output, world)
  }

  @FXML
  def deserializeWorldFromFile(e: ActionEvent): Unit = {
    var fileChooser: FileChooser = new FileChooser()
    var jsonFilter: FileChooser.ExtensionFilter = new FileChooser.ExtensionFilter("JSON files (*.json)", "*.json");

    fileChooser.setTitle("Open world...")
    fileChooser.getExtensionFilters().add(jsonFilter)

    var input: File = fileChooser.showOpenDialog(root.getScene().getWindow())
    if (!input.getName().contains("."))
      input = new File(input.getAbsolutePath() + ".json")

    world = serializationService.deserializeWorld(input)
    // FIXME should be defined as closure or sth
    for(agent <- world.agents)
      agent.fxCircle.setOnMousePressed((_: MouseEvent) => agentPanel.agent.setValue(agent))
  }

  @FXML
  def handleMouseMovementWhenDrawing(e: MouseEvent) =
    obstacleService.handleLineUpdate(new javafx.geometry.Point2D(e.getX, e.getY))

  def initialize(location: URL, resources: ResourceBundle) = {

    obstacleService.canvas = rootCanvas
    serializationService.canvas = rootCanvas
    serializationService.agentPanel = agentPanel

    val prefWidth = 700.0
    val prefHeight = 700.0

    rootCanvas.setPrefWidth(prefWidth)
    rootCanvas.setPrefHeight(prefHeight)

    rootCanvas.setOnMousePressed((event: MouseEvent) => mouseMode.get match {

      case Mouse.AddAgentMode =>
        
        val agent = new Agent("agent01", 10, Color.PINK, event.getX, event.getY, event.getX, event.getY)
        agent.fxCircle.setOnMousePressed((_: MouseEvent) => agentPanel.agent.setValue(agent))

        agent.world = world

        rootCanvas.getChildren.addAll(agent.fxCircle, agent.fxTarget, agent.fxPolyline)
        agentPanel.agent setValue agent

        world.agents += agent

        mouseMode set Mouse.SetTargetMode

      case Mouse.SetTargetMode if Option(event.getTarget) exists (_ == rootCanvas) =>
        
        Option(agentPanel.agent.get) map { agent =>
          agent.targetX set event.getX
          agent.targetY set event.getY
        }

      // agent is still selected, stay in targeting mode

      case Mouse.AddObstacleMode =>

        val obstacle = obstacleService.handleObstacleCreationClick(event)

        if (obstacle != null) {
          world.obstacles += obstacle
          // revert previous mode after obstacle is added
          mouseMode set prevMode.get
        }

      case _ =>
    })
  }

  def runSimluation(): Unit = {

    import javafx.animation._

    val maxPathStripSize = (world.agents map { agent => agent.fxCircle.getRadius }).min / 10.0

    import collection.JavaConversions._

    def stripPath(points: Seq[java.lang.Double], stripSize: Double) = points.iterator.sliding(4, 4).toList.map {
      case x1 :: y1 :: x2 :: y2 :: _ =>

        val length = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))

        val divideInto = Math.ceil(length / stripSize)

        (0 to divideInto.intValue).map { n =>

          val t = n / divideInto
          Common.Point2D(
            x1 * (1 - t) + x2 * t,
            y1 * (1 - t) + y2 * t)

        }.iterator.sliding(2).toList.map {
          case p1 :: p2 :: _ => Common.Segment2D(p1, p2)
        }

    } flatten

    val initialAgents = world.agents map { agent =>
      val strippedPath = stripPath(agent.fxPolyline.getPoints, maxPathStripSize)
      (agent, strippedPath)
    }

    // now we got a sequence of agents and their paths (stripped into small pieces)

    // ***********************************
    // sequential simulation logic
    // ***********************************

    val sequentialSimulationControl = new SimulationControl {

      val singleStepTimeMillis = 10

      def runSimulation(world: World, initialState: SimulationState) = {

        val longestPathSize = initialState.maxBy(_._2.size)._2.size

        val extState = initialState map {
          case (agent, path) =>
            val circle = new Circle(
              agent.fxCircle.getCenterX,
              agent.fxCircle.getCenterY,
              agent.fxCircle.getRadius,
              agent.fxCircle.getFill)
            circle.setOpacity(0.8)

            val targetPoint = path.last.p2
            val staticPath = Common.Segment2D(targetPoint, targetPoint)

            rootCanvas.getChildren.add(circle)
            (agent, circle, path.padTo(longestPathSize, staticPath))
        }

        simulationStep(extState)
      }
     
      def findEscapePoint(currentPos: Common.Point2D, size: Double, obstacles: Seq[Obstacle]): Common.Point2D = {
        var escapePoint: Common.Point2D = currentPos
        // got 8 possible escape points - try all of them
        for (possibility <- 1 to 8) {
          possibility match {
            case 1 =>
              escapePoint = new Common.Point2D(currentPos.x1 + size, currentPos.x2)
            case 2 =>
              escapePoint = new Common.Point2D(currentPos.x1 - size, currentPos.x2)
            case 3 =>
              escapePoint = new Common.Point2D(currentPos.x1, currentPos.x2 + size)
            case 4 =>
              escapePoint = new Common.Point2D(currentPos.x1, currentPos.x2 - size)
            case 5 =>
              escapePoint = new Common.Point2D(currentPos.x1 + size, currentPos.x2 + size)
            case 6 =>
              escapePoint = new Common.Point2D(currentPos.x1 - size, currentPos.x2 + size)
            case 7 =>
              escapePoint = new Common.Point2D(currentPos.x1 + size, currentPos.x2 - size)
            case 8 =>
              escapePoint = new Common.Point2D(currentPos.x1 - size, currentPos.x2 - size)
          }
          if (!pointInsideAnyObstacle(escapePoint, obstacles))
            return escapePoint
        }
        return escapePoint
      }
      
      def pointInsideAnyObstacle(point: Common.Point2D, obstacles: Seq[Obstacle]): Boolean = {
        for(potentialObstacle <- obstacles) 
            if(pointInsideParticularObstacle(point, potentialObstacle))
              return true
        return false
      }

      def pointInsideParticularObstacle(point: Common.Point2D, obstacle: Obstacle): Boolean = {
        var j = obstacle.corners.length - 1
        var oddNodes = false

        for (cornerNum <- 0 to obstacle.corners.length) {
          if ((obstacle.corners.get(cornerNum).getY() < point.x2 && obstacle.corners.get(j).getY() >= point.x2
            || obstacle.corners.get(j).getY() < point.x2 && obstacle.corners.get(cornerNum).getY() >= point.x2)
            && (obstacle.corners.get(cornerNum).getX() <= point.x1 || obstacle.corners.get(j).getX() <= point.x1))
          {
            oddNodes^= (obstacle.corners.get(cornerNum).getX()
                + (point.x2 - obstacle.corners.get(cornerNum).getY())
                / (obstacle.corners.get(j).getY() - obstacle.corners.get(cornerNum).getY()) 
                * (obstacle.corners.get(j).getX() - obstacle.corners.get(cornerNum).getX()) 
                < point.x1) 
          }
          j = cornerNum
        }
        return oddNodes
      }

      def simulationStep(currentState: Seq[(Agent, Circle, AgentPath)]): Unit = {

        // move each agent along its path

        // each agents path consists of same number of elements
        // thus there is no need for agent removals from currentState
        // between calls

        if (currentState.forall(_._3.size > 0)) {

          val nextState = currentState.map {
            case (agent, node, path) =>

              val pos = path.head.p2

              val foes = currentState.filter {
                case (otherAgent, _, otherPath) =>
                  val otherPos = otherPath.head.p2
                  (otherAgent != agent) &&
                    (Common.Geom.dist(pos, otherPos) < 10 * agent.size.get) &&
                    (otherAgent.size.get > agent.size.get ||
                      (otherAgent.size.get == agent.size.get && otherAgent.hashCode > agent.hashCode))
              }

              if (foes.nonEmpty) {

                // there is bigger aggent on my way, searching for a new path

                val moreObstacles = foes.map {
                  case (otherAgent, _, otherPath) =>
                    val circle = agent.getCircle(otherPath.head.p2, 1 * otherAgent.size.get, 4);
                    val mapped = circle.map(p => new javafx.geometry.Point2D(p.x1, p.x2))
                    Obstacle(mapped.toList.to[ListBuffer])
                } toSeq

                val escapePoint = findEscapePoint(pos, agent.size.get(), moreObstacles)
                agent.calcPathOnDemand(pos.x1, pos.x2, moreObstacles)

                val strippedPath = stripPath(agent.fxPolyline.getPoints, maxPathStripSize)

                val step = strippedPath.head

                val pathTransition = new PathTransition();
                pathTransition.setDuration(javafx.util.Duration.millis(10))
                pathTransition.setPath(new javafx.scene.shape.Line(step.p1.x1, step.p1.x2, step.p2.x1, step.p2.x2))
                pathTransition.setNode(node)
                pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
                pathTransition.setCycleCount(1)
                pathTransition.play()

                (agent, node, strippedPath.tail)

              } else {

                val step = path.head

                val pathTransition = new PathTransition();
                pathTransition.setDuration(javafx.util.Duration.millis(singleStepTimeMillis))
                pathTransition.setPath(new javafx.scene.shape.Line(step.p1.x1, step.p1.x2, step.p2.x1, step.p2.x2))
                pathTransition.setNode(node)
                pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
                pathTransition.setCycleCount(1)
                pathTransition.play()

                (agent, node, path.tail)
              }
          }

          val pause = new PauseTransition(Duration.millis(singleStepTimeMillis))
          pause.setOnFinished((_: ActionEvent) => {
            simulationStep(nextState)
          })
          pause.play()

        } else {
          currentState.foreach(x => rootCanvas.getChildren.remove(x._2))
        }
      }
    }

    // ***********************************
    // parallel simulation logic
    // ***********************************

    val parallelSimulationControl = new SimulationControl {

      // each agent is responsible for updating his position here
      // this is accessed by all agents to determine distance from foes
      lazy val positions = scala.collection.mutable.Map[Agent, Common.Point2D]()

      def runSimulation(world: World, initialState: SimulationState) = {

        initialState map {
          case (agent, path) =>
            val circle = new Circle(
              agent.fxCircle.getCenterX,
              agent.fxCircle.getCenterY,
              agent.fxCircle.getRadius,
              agent.fxCircle.getFill)
            circle.setOpacity(0.8)

            positions(agent) = Common.Point2D(circle.getCenterX, circle.getCenterY)

            rootCanvas.getChildren.add(circle)
            agentStep(agent, circle, path)
        }
      }

      def agentStep(agent: Agent, node: Circle, path: Seq[Common.Segment2D]): Unit = path match {

        case Nil =>

          // after reaching the target wait 2 secs. and remove 'ghost' node
          val pause = new PauseTransition(Duration.millis(2000))
          pause.setOnFinished((_: ActionEvent) => {
            rootCanvas.getChildren.remove(node)
          })
          pause.play()

        case step :: rest =>

          val foes = positions.filter {
            case (otherAgent, otherPosition) =>

              // foe agent is closer than 4x my size
              // and is 'bigger', stop moving

              (otherAgent != agent) &&
                (Common.Geom.dist(positions(agent), otherPosition) < 10 * agent.size.get) &&
                (otherAgent.size.get > agent.size.get ||
                  (otherAgent.size.get == agent.size.get && otherAgent.hashCode > agent.hashCode))
          }

          if (foes.nonEmpty) {

            // there is bigger aggent on my way, searching for a new path

            val moreObstacles = foes.map {
              case (otherAgent, otherPosition) =>
                val circle = agent.getCircle(otherPosition, 1 * otherAgent.size.get, 4);
                val mapped = circle.map(p => new javafx.geometry.Point2D(p.x1, p.x2))
                Obstacle(mapped.toList.to[ListBuffer])
            } toSeq

            val myPosition = positions(agent);

            agent.calcPathOnDemand(myPosition.x1, myPosition.x2, moreObstacles)

            val strippedPath = stripPath(agent.fxPolyline.getPoints, maxPathStripSize)

            val step = strippedPath.head

            val pathTransition = new PathTransition();
            pathTransition.setDuration(javafx.util.Duration.millis(10))
            pathTransition.setPath(new javafx.scene.shape.Line(step.p1.x1, step.p1.x2, step.p2.x1, step.p2.x2))
            pathTransition.setNode(node)
            pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
            pathTransition.setCycleCount(1)
            pathTransition.setOnFinished((_: ActionEvent) => {
              positions(agent) = Common.Point2D(step.p2.x1, step.p2.x2)
              agentStep(agent, node, strippedPath.tail)
            })
            pathTransition.play()

          } else {

            val pathTransition = new PathTransition();
            pathTransition.setDuration(javafx.util.Duration.millis(10))
            pathTransition.setPath(new javafx.scene.shape.Line(step.p1.x1, step.p1.x2, step.p2.x1, step.p2.x2))
            pathTransition.setNode(node)
            pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
            pathTransition.setCycleCount(1)
            pathTransition.setOnFinished((_: ActionEvent) => {
              positions(agent) = Common.Point2D(step.p2.x1, step.p2.x2)
              agentStep(agent, node, rest)
            })
            pathTransition.play()
          }
      }
    }

    //		parallelSimulationControl.runSimulation(world, initialAgents)
    sequentialSimulationControl.runSimulation(world, initialAgents)

  }

}
