package agenty.gui.controllers

import java.io.IOException
import javafx.fxml.FXMLLoader
import javafx.scene.layout.GridPane
import javafx.fxml.FXML
import javafx.scene.control.TextField
import javafx.fxml.Initializable
import java.net.URL
import java.util.ResourceBundle
import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import agenty.gui.models.Agent
import javafx.beans.value.ObservableValue
import javafx.beans.value.ChangeListener
import agenty.gui.fxextensions.LambdaConversions._
import agenty.gui.fxextensions.BindingOperators._
import agenty.gui.controls.SpinnerField
import javafx.scene.control.ColorPicker
import javafx.util.converter.NumberStringConverter

class AgentPanel extends GridPane with Initializable {
	
	@FXML var tfName: TextField = _
	@FXML var sfSize: SpinnerField = _
	@FXML var cpColor: ColorPicker = _
	@FXML var tfXCoord: TextField = _
	@FXML var tfYCoord: TextField = _
	@FXML var tfXDest: TextField = _
	@FXML var tfYDest: TextField = _
	
	val agent: ObjectProperty[Agent] = new SimpleObjectProperty(null)
		
	val fxmlLoader = new FXMLLoader(getClass().getResource("/agenty/gui/views/AgentPanel.fxml"))
	fxmlLoader.setRoot(this)
	fxmlLoader.setController(this)
	
	try {
		fxmlLoader.load()
	} catch {
		case ex: IOException => throw new RuntimeException(ex)
	}
	
	def initialize(location: URL, resources: ResourceBundle) = {
	}
	
	agent.addListener((_: ObservableValue[Agent], oldAgent: Agent, newAgent: Agent) => {

		Option(oldAgent) map { oldAgent =>
			oldAgent.fxCircle.getStyleClass remove "agentSelected"
			
			tfName.textProperty unbindBidirectional oldAgent.name
			sfSize.valueProperty unbindBidirectional oldAgent.size
			cpColor.valueProperty unbindBidirectional oldAgent.color
			
			tfXCoord.textProperty unbindBidirectional oldAgent.centerX
			tfYCoord.textProperty unbindBidirectional oldAgent.centerY
			tfXDest.textProperty unbindBidirectional oldAgent.targetX
			tfYDest.textProperty unbindBidirectional oldAgent.targetY
		}
		
		Option(newAgent) map { newAgent =>
			newAgent.fxCircle.getStyleClass add "agentSelected"
			
			tfName.textProperty bindBidirectional newAgent.name
			sfSize.valueProperty bindBidirectional newAgent.size
			cpColor.valueProperty bindBidirectional newAgent.color
			
			val sc = new NumberStringConverter
			tfXCoord.textProperty.bindBidirectional(newAgent.centerX, sc)
			tfYCoord.textProperty.bindBidirectional(newAgent.centerY, sc)
			tfXDest.textProperty.bindBidirectional(newAgent.targetX, sc)
			tfYDest.textProperty.bindBidirectional(newAgent.targetY, sc)
		}
	})
}
