package agenty.utils

import Common._
import Predicates._
import scala.language.implicitConversions

object VisibilityGraph {

  implicit def PointToArray(p: Point2D) = Array[Double](p.x1, p.x2)

  type Predicate = (Array[Double], Array[Double], Array[Double]) => Double

  implicit class Polygon2DWithSegments(val poly: Polygon2D) {
    def segments = {
//      println(s"calculating segments from points: ${poly.points.length}")
      poly.points.slidingMod(2, 1) map (p => Segment2D(p(0), p(1))) toList
    }
  }

  implicit class Segment2DWithIntersectionTest(val seg1: Segment2D) {
    def intersects(seg2: Segment2D, pred: Predicate = Predicates.orient2dfast): Boolean = {

      val s = List(seg1, seg2)
      pred(s(0).p1, s(0).p2, s(1).p1) * pred(s(0).p1, s(0).p2, s(1).p2) < 0 &&
        pred(s(1).p1, s(1).p2, s(0).p1) * pred(s(1).p1, s(1).p2, s(0).p2) < 0
    }
  }

  def visibilityGraph(freePoints: Seq[Point2D], obstacles: Seq[Polygon2D]): List[Segment2D] = {
    implicit def PointToArray(p: Point2D) = Array[Double](p.x1, p.x2)

    val result = collection.mutable.HashSet[Segment2D]()

    val allPoints = freePoints.toList ++ (obstacles.map(_.points)).flatten.toList
    val allSegments = obstacles map (_.segments) flatten

    val excluded = collection.mutable.HashSet[Point2D]()

    // find all points which are inside of some other polygon (all orients have the same sign)
    allPoints foreach (point => {
      val poly = obstacles.find(_.points.contains(point))
      if (poly != None) {
        obstacles foreach (obstacle => {
          if (!obstacle.equals(poly)) {
            val dets: List[Double] = obstacle.segments.map(s => Predicates.orient2dfast(s.p1, s.p2, point))

            if (!dets.exists(_ <= 0) || !dets.exists(_ >= 0)) {
              excluded += point
            }
          }
        })
      }
    })

//    println("number of excluded points " + excluded.size)

    allPoints foreach (point => {
      allPoints foreach (target => {
        val seg = Segment2D(point, target)
        if (!excluded.contains(point) && !excluded.contains(target)) {
          if (!allSegments.exists(_.intersects(seg)) && // segments shall not intersect
            ((obstacles.find(_.points.contains(point)) != obstacles.find(_.points.contains(target)) || // points should belong to different polygons
              (freePoints.contains(point) && freePoints.contains(target))))) // start - end case
            result += seg
        }
      })
    })

    val fixedBorders = collection.mutable.HashSet[Segment2D]()

    // if any border intersects other border, add their intersection point as well
    // as those two of their ends which are not excluded (are outside of another polygon) to the graph
    // if a border doesn't intersect any other, add it to the graph if neither of its end is excluded
    allSegments foreach (segment => {
      var intersectsAnyOther = false
      allSegments foreach (other => {
        if (segment != other) {
          if (segment.intersects(other)) {

            intersectsAnyOther = true
            val l1 = Line2D.fromSegment(segment)
            val l2 = Line2D.fromSegment(other)
            val x = (l1.intercept - l2.intercept) / (l2.slope - l1.slope)
            val intersPoint = Point2D(x, l1.atX1(x))

            if (excluded.contains(segment.p1)) {
              fixedBorders += Segment2D(segment.p2, intersPoint)
            }
            if (excluded.contains(segment.p2)) {
              fixedBorders += Segment2D(segment.p1, intersPoint)
            }
            if (excluded.contains(other.p1)) {
              fixedBorders += Segment2D(other.p2, intersPoint)
            }
            if (excluded.contains(other.p2)) {
              fixedBorders += Segment2D(other.p1, intersPoint)
            }
          }
        }
      })
      if (!intersectsAnyOther && !excluded.contains(segment.p1)
        && !excluded.contains(segment.p2)) fixedBorders += segment
    })

    result.toList ++ fixedBorders
    //(allSegments filter (s => !excluded.contains(s.p1) ||
    //!excluded.contains(s.p2))) // it is possible to move along polygon border
  }

  def shortestPath(edges: Seq[Segment2D], startP: Point2D, endP: Point2D): List[Segment2D] = {

    val allPoints = edges.map(e => List(e.p1, e.p2)).flatten.toSet

    require(allPoints.contains(startP))
    require(allPoints.contains(endP))

    val g = new dijkstra.WeightedGraph(1)

    val nodes = allPoints.map(p => p -> g.addNode).toMap
    val invNodes = nodes map (_.swap)

    edges foreach (e => nodes(e.p1) connectWith nodes(e.p2) setWeight ((Geom.dist(e.p1, e.p2) * 1000).toInt))

    val (start, target) = (nodes(startP), nodes(endP))
    val djk = new dijkstra.Dijkstra[g.type](g)
    djk.stopCondition = (S, D, P) => !S.contains(target)
    val (distance, path) = djk.compute(start, target)
    
    try {
    	
    	var shortest = List(target)
    	
    	while (shortest.head != start)
      		shortest ::= path(shortest.head)
      		
      	val nods: List[List[g.Node]] = shortest.sliding(2).toList
    	nods.map(pp => Segment2D(invNodes(pp(0)), invNodes(pp(1)))).toList
    		
    } catch {
    	case _: Exception => List()
    }
  }

  implicit class PowInt(base: Int) {
    def **(exp: Int): Int = math.pow(base, exp).round.toInt
  }
}
