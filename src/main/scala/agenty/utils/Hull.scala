package agenty.utils

import Common._
import Predicates.{orient2dfast => orient2dexact}

object Hull {
  implicit def PointToArray(p: Point2D) = Array[Double](p.x1, p.x2)

  type Predicate = (Array[Double], Array[Double], Array[Double]) => Double

  def graham(
    data: Seq[Point2D],
    pred: Predicate) = {

    val p0 = (data.filter(p => p.x2 == data.minBy(_.x2).x2)).minBy(_.x1)
    val rest = (data diff List(p0)).sortWith((b, c) => // pred(p0, b, c) <  0 )
      if (pred(p0, b, c) == 0) Geom.dist(p0, b) < Geom.dist(p0, c) else pred(p0, b, c) < 0)

    val stack = new collection.mutable.Stack[Point2D]()
    stack.push(p0, rest(0), rest(1))

    var i = 3

    while (i < rest.length) {

      val pi = rest(i)

      //                      println(s"analyzing point $pi")
      //                      println(s"stack haz ${stack.size} elements: $stack")

      if (stack.size < 2 || pred(stack(0), stack(1), pi) > 0) { stack push pi; i += 1 }
      else
        stack.pop
    }

    stack.toList
  }

  def jarvis(
    data: Seq[Point2D],
    pred: Predicate = orient2dexact) = {

    var curr = (data.filter(p => p.x2 == data.minBy(_.x2).x2)).minBy(_.x1)
    var next = curr

    val hull = collection.mutable.ArrayBuffer[Point2D]()

    do {
      //                      println(s"analyzing point $curr")
      hull += curr

      data foreach (p => {
        val orient = pred(curr, next, p)

        if (orient > 0 || (orient == 0 && Geom.dist(curr, p) > Geom.dist(curr, next)))
          next = p
      })

      curr = next

    } while (next != hull(0))

    hull.toList
  }
}
