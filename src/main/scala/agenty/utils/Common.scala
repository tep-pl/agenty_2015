
package object Common {
	
	import Math._

	implicit class PowOp(base: Double) {
		def **(exp: Double) = math.pow(base, exp)
	}
	
	case class Precision(val p:Double)
	
	implicit class DoubleWithAlmostEquals(val d: Double) extends AnyVal {
		def ~=(d2: Double)(implicit p: Precision) = (d - d2).abs < p.p
	}
	
	implicit val prec = Precision(0.000001) 

	case class Point2D(val x1: Double, val x2: Double) {
		override def toString() = s"Point2D($x1,$x2)"
		def +(v: Vector2D) = Point2D(x1+v.v1, x2+v.v2)
		override def equals(arg: Any) = arg match {
			case Point2D(y1, y2) => (x1 ~= y1) && (x2 ~= y2)
			case _ => false
		}
	}

	case class Vector2D(val v1: Double, val v2: Double) {
		override def toString() = s"Vector2D($v1,$v2)"
		def length() = sqrt(v1 ** 2 + v2 ** 2)
		def normal() = Vector2D(v1/length, v2/length)
		def *(k: Double) = Vector2D(k*v1, k*v2)
	}
	
	object Vector2D {
		
		def fromPoints(p1: Point2D, p2: Point2D)
			= Vector2D(p2.x1-p1.x1, p2.x2-p1.x2)
	}
	
	case class Segment2D(val p1: Point2D, val p2: Point2D) {
		override def toString() = s"Segment2D($p1,$p2)"
	}

	case class Line2D(
		val slope: Double,
		val intercept: Double) {

		def atX1(x1: Double) = slope * x1 + intercept
		def atX2(x2: Double) = (x2 - intercept) / slope
	}

	object Line2D {
		def fromPoints(
			a: Point2D,
			b: Point2D) = {
			// prosta ab: x2 = p x1 + q
			val p = (b.x2 - a.x2) / (b.x1 - a.x1)
			val q = a.x2 - p * a.x1
			new Line2D(p, q)
		}
		
		def fromSegment(s: Segment2D) = fromPoints(s.p1, s.p2)
	}
	
	implicit class SeqWithModSliding[T](val x: Seq[T]) {
		def slidingMod(n: Int, s: Int = 1) = (List(x.last) ++ x ++ List(x.head)).sliding(n, s)
	}
	
	case class Polygon2D(val points: List[Point2D])

	object Geom {

		def dist(p1: Point2D, p2: Point2D) = sqrt((p1.x1 - p2.x1) ** 2 + (p1.x2 - p2.x2) ** 2)
	}

}
