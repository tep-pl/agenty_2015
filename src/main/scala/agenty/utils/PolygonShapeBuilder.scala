package agenty.utils

import javafx.scene.shape.{Polygon, Polyline}
import Common._

object PolygonShapeBuilder {
	
	object State extends Enumeration {
		val Idle, Building = Value
	}
}

class PolygonShapeBuilder {
	
	private var _polygon: Polygon = null
	private var _polylin: Polyline = null
	
	def polygon  = _polygon
	def polyline = _polylin
	
	var _points = collection.mutable.ListBuffer[Point2D]()
	var _state = PolygonShapeBuilder.State.Idle
	
	def points = _points.toList
	def state = _state
	
	def addPoint(p: Point2D) = {
		
		_state = PolygonShapeBuilder.State.Building
		
		_points += p
		_polygon.getPoints.addAll(p.x1, p.x2)
		
		val pts = _polylin.getPoints
		val (p0, p1) = (pts.get(pts.size-2), pts.get(pts.size-1))
		pts.remove(pts.size-2, pts.size)
		pts.addAll(p.x1, p.x2, p0, p1)
	}
	
	def updateLine(p: Point2D) = {
		val pts = _polylin.getPoints
		pts.remove(pts.size-2, pts.size)
		pts.addAll(p.x1, p.x2)
	}
	
	
	def reset() = {
		_polygon = new Polygon
		_polylin = new Polyline
		_points = collection.mutable.ListBuffer[Point2D]()
		_state = PolygonShapeBuilder.State.Idle
		
		_polylin.getPoints.addAll(0, 0)
	}
	
	reset
}
