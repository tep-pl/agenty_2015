package agenty.utils

object Predicates {
	
	def orient2dfast(pa: Array[Double], pb: Array[Double], pc: Array[Double]): Double = {
		
		val acx = pa(0) - pc(0)
		val bcx = pb(0) - pc(0)
		val acy = pa(1) - pc(1)
		val bcy = pb(1) - pc(1)
		
		acx * bcy - acy * bcx
	}
}
